package com.example.day256;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.netty.http.server.HttpServer;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@SpringBootApplication
public class Day256Application {

	public static void main(String[] args)
			throws InterruptedException {
		HttpHandler httpHandler = RouterFunctions.toHttpHandler(getRouter());

		HttpServer
				.create()
				.host("localhost")
				.port(8080)
				.handle(new ReactorHttpHandlerAdapter(httpHandler))
				.bindNow()
				.onDispose()
				.block();

		Thread.currentThread().join();
	}

	static RouterFunction getRouter() {
		HandlerFunction<ServerResponse> hello = request -> {
			int year = Integer.parseInt(request.queryParam("year").orElse("2019"));
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.set(Calendar.DAY_OF_YEAR, 256);
			calendar.set(Calendar.YEAR, year);
			return ok().contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromValue(new Answer
					(200, new SimpleDateFormat("dd/MM/yy").format(calendar.getTime()))));
		};
		return route(GET("/"), hello);
	}
}

