package com.example.day256;
import java.util.Objects;

public class Answer {
    private int errorCode;
    private String dataMessage;

    public Answer(){
    }

    public Answer(int errorCode, String dataMessage) {
        this.errorCode = errorCode;
        this.dataMessage = dataMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public void setDataMessage(String dataMessage) {
        this.dataMessage = dataMessage;
    }

    public String getDataMessage() {
        return dataMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return errorCode == answer.errorCode &&
                Objects.equals(dataMessage, answer.dataMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(errorCode, dataMessage);
    }
}
