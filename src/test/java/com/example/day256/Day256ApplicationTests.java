package com.example.day256;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest
class Day256ApplicationTests {
	private final WebTestClient webTestClient =
			WebTestClient
					.bindToRouterFunction(Day256Application.getRouter())
					.build();

	@Test
	public void jsonPage_WhenRequested() {
		webTestClient.get().uri("/?year=2019").exchange()
				.expectStatus().is2xxSuccessful()
				.expectHeader().contentType(MediaType.APPLICATION_JSON)
				.expectBody(Answer.class)
				.isEqualTo(new Answer(200, "13/09/19"));

		webTestClient.get().uri("/?year=2020").exchange()
				.expectStatus().is2xxSuccessful()
				.expectHeader().contentType(MediaType.APPLICATION_JSON)
				.expectBody(Answer.class)
				.isEqualTo(new Answer(200, "12/09/20"));
	}
}
